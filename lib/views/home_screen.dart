import 'package:flutter/material.dart';
import 'dart:convert';
import '../models/pizza.dart';

class HomeScreen extends StatelessWidget {

  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Home'),),
      body: Container(
        child: FutureBuilder(
          future: readJsonFile(context),
          builder: (BuildContext context, AsyncSnapshot<List<Pizza>> pizzas) {
            return ListView.builder(
                itemCount: pizzas.data?.length ?? 0,
                itemBuilder: (BuildContext context, int index) {
                  var itemData = pizzas.data![index];
                  return ListTile(
                    title: Text(itemData.pizzaName),
                    subtitle: Text("${itemData.description } \$ ${itemData.price}"),
                  );
                }
            );
          },
        ),
      ),
    );
  }

  Future<List<Pizza>> readJsonFile(context) async {
    String myString = await DefaultAssetBundle.of(context)
        .loadString('assets/pizza_list.json');

    List myMap = jsonDecode(myString);
    List<Pizza> myPizzas = [];
    myMap.forEach((dynamic pizza) {
      Pizza myPizza = Pizza.fromJson(pizza);
      // Pizza myPizza = Pizza.fromJsonOrNull(pizza);
      myPizzas.add(myPizza);
    });

    return myPizzas;
  }
}